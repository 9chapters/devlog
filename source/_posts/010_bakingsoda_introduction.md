---
title: Baking Soda Introduction
date: 2019-07-09 10:00:00
tag:
   - Baking Soda
   - archive
---

![](/devlog/images/bs.png)

## Why Baking Soda

>  Because it has long been known and is widely used  -- from Wikipedia

## What Baking Soda Is

Baking soda is a general indexer for any services in JSON format. When a service is provided, the baking soda will crawl the document back as raw data and store in a database. Then, it will automatically analyze documents for parsing and indexing raw data as meaningful data for further analysis.

Usually, people will target special third party service to develop an indexer/crawler. If that service is changed, the code of indexer/crawler must follow. With the baking soda, the code change can be prevented. The baking soda can help us to detect the change of service and perform the further step for indexing data which can be automatically or manually.

If we look closely into the baking soda, it can be treated as three parts: *crawler*, *analyzer*, *indexer*. The following sections will give you a high level overveiw of how baking soda work.

### Crawler

The crawler will visit the target source, fetch the data and save it in the database as raw data. As a web crawler, we can provide baking soda a _seed_ and the way to grow the seed. The seed is a just string template of URL. Using Tezos RPC as an example, it looks like:

```bash
  "/chains/main/blocks/$block.level$"
```

The `$block.level$` is substitution placeholder for the block level. We can define a data in Haskell to describe the placeholder.

```haskell
data TzBlock = TB { level :: Int }
```

This data need to be the instance of `Iso` typeclass. `zero :: a -> a` in `Iso` define the start point of seed. `succ :: a -> a` define how to generate the next seed.

```haskell
class Iso a where
    zero :: a -> a
    succ :: a -> a
```

In the example of `TzBlock`, `zero` would be 0 and `succ` is adding one!

```haskell
instance Iso TzBlock where
    zero _ = TB 0
    succ tb = tb { level = level tb + 1 }

```

### Analyzer

The analyzer will analysis the documents which are the raw data from database and generate a configuration for customizing settings if need and env. The configuration and env will be loaded back while indexing time. The configuration is the rules based and used to describe how the target value of JSON is indexed in a database. The target value of JSON will be analyzed and generate by the analyzer. There are default settings in the configuration for describing data saving location, parsing JSON. The setting can be manually by the user for the futher perform setting or customized needs. Based on the configuration, baking soda can produce the SQL for creating or altering table.

### Indexer

According to settings of configuration, the indexer will parse JSON and index values into the database. If the rules in the configuration are too many, we can separate the rules in the different configuration files and run indexers for each configuration to speed up the index time.

## TODO

The baking soda is ready for alpha release. There are some TODO tasks:

- document for customizing configuations
- redesign/provide DSL
- index update for data change
- monitoring/modeling for tracking change

## Repo

- [Baking Soda](https://gitlab.com/9chapters/baking-soda). The core library.
- [Baking Soda For Tezos Ecosystem](https://gitlab.com/9chapters/baking-soda-tezos). The client of baking soda for indexing Tezos node and MyTezosBaker.
