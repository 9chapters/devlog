---
title: Identity, Committee and Commission
date: 2019-04-08 22:00:00
tag:
   - identity
   - commission
   - archive
---

## the identity

An _on-chain identity_ is a simple idea. It can be directly _mapped_ to a data, a resource, a contract, or, even a private key (or its hash). It's entirely up to how the developer want to use or create it.

However, no matter what we think it is, one of the most important thing about _identity_ is that, it reflects some kind of **ownership**. Either there are already some informaiton on an _identity_; or, an _identity_ owns or links to other recourses. Whoever owns that _identity_ will also own the information and resources owned by it. So it crucial to define a mechanism to manage an idendity in an efficient and flexible way.

### on-chain managment

One way to public manage an operation towards an _identity_ is setting a **committee** inside it. Each member of such _committee_ is another _identity_ or _account_ (corresponding to real human). All operation, like modification or ownershi changing, requires the approval from _committee_.

The onwer itself can be a member of _committee_ or not. And every committee member can be entitled to different permission level.

The _committee_ is a rich design that can make lots of identity operation possible. But we still answer a fundamental question: how to get a broad consensus from a committee? The answer is simple: _democracy_!

### voting and HOV

One simple way to ontain a broad consensus from a committee is _voting_. So the challenge here is to realize an on-chain voting system. To be able to do so, one can simply embed voting mechanism into each _identity_. Which means, one will add several entries onto an _identity_. This increases the degree of indenpendancy amoung contracts; yet, it also increases the complexity of writing a contract.

So, introducing: **hov**, a high-order voting system/contract. The idea of _hov_ is simple: it's an indenpendent contract which provides the capability of originate voting contract whenever a voting demand is issued.

### overvew

With _identity:x_ and _commission_ are all predefined contracts. The following illustration shows how an _identity_ and **commisson**, a _hov_ contract, can interact with each other and form a procress of _identity modification_.

```mermaid
graph TB
   style X fill:#CCC
   style VR fill:#CCC
   style IdX fill:#FFA
   style C fill:#AAF

   X[owner of X] -.-> |1. request for<br> modification| IdX
   VR[committee<br> members of X] -.-> |4. voting| VB

   IdX --> |2. apply| C
   VB --> |5. trigger| Op1

   subgraph ""
      C(commission)
      VB(voting booth)
      C ==> |3. originate| VB
   end

   subgraph ""
      IdX(identity:x)
      Op1(op trigger)
      IdX ==> |2. originate| Op1
      Op1 --> |6. trigger to<br> proceed<br> modification| IdX
   end
```

Please go to [<i class="fab fa-gitlab"></i>TSA/identity/v0.1](https://gitlab.com/tezos-southeast-asia/identity/commit/aacafe694f606e2edbb884cd955670aecaf0576c) to see more detail.
