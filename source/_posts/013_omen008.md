---
title: An omen of Nine Chapters (008)
date: 2019-08-15 14:00:00
tag:
   - archive
   - omen
   - Tezos.ID
---

> While you're turning the pages of the Nine Chapters,
> you saw a line sparkling. Suddenly you learn something new from nowhere!

## Updated

Another week has been passed since [the last _omen_](https://9chapters.gitlab.io/devlog/2019/08/07/013_omen007/). We're happy to update with you ...

+ Tezos.ID
  + SEO pagination for blocks/transactions/endorsements/delegations/originations page and its url query


## Upcoming


+ Tezos.ID
  + Quick Install doc
