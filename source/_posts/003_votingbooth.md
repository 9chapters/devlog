---
title: The VotingBooth
date: 2019-04-20 22:05:00
tag:
   - identity
   - hov
   - archive
---

## Overview of VotingBooth

Let's continue developing of [_commission_](https://tezos-southeast-asia.gitlab.io/identity-devlog/2019/04/08/commi-star/).

One of the most crucial part of _commission_ is the contract we call as **voting booth**. A _voting booth_ is

1. originated by the _commission_ contract,
2. containing a name list of valid voter, i.e. _committee_, and,
3. containing a specific resolving rule which determines if this voting is over.

```mermaid
graph LR
   style VR fill:#CCC
   style C fill:#AAF

   C(commission)
   VB(voting booth)
   C ==> |1. originate| VB
   VR[committee] -.-> |2. voting| VB
   VB --> |3. trigger| Op(following<br> contract)
```

### Design

A _voting booth_ as a contract contains at least **two entries**: the one is an entry for _voting_; and, the other one is for being triggered to _resolve_ the voting and inform the following contract. This seems-unnecessary entry is the key to implement a time-restricted voting rule.

Another key is, this contract is actually working as a template of the whole _hov_ system. We don't originate it by our own hand. Instead, all _voting booth_ will be originated by the indenpendent contract _commission_ as its name indicates.

### Michelson code

```
# VotingBooth

parameter
  (or :_entries
     (or :tTriple %_Liq_entry_vote
        (unit %Undefined)
        (or (unit %Positive) (unit %Negative)))
     (unit %_Liq_entry_resolve));
storage
  (pair :storage
     (map %sheet
        address
        (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
     (pair (lambda %resolver
              (map address (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
              (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
           (pair (or :tTriple %result (unit %Undefined) (or (unit %Positive) (unit %Negative)))
                 (address %bridge))));
code { DUP ;
       DIP { CDR @storage_slash_1 } ;
       CAR @parameter_slash_2 ;
       LAMBDA @resolve
         (pair :storage
            (map %sheet
               address
               (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
            (pair (lambda %resolver
                     (map address (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
                     (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
                  (pair (or :tTriple %result (unit %Undefined) (or (unit %Positive) (unit %Negative)))
                        (address %bridge))))
         (pair :storage
            (map %sheet
               address
               (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
            (pair (lambda %resolver
                     (map address (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
                     (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))))
                  (pair (or :tTriple %result (unit %Undefined) (or (unit %Positive) (unit %Negative)))
                        (address %bridge))))
         { RENAME @s_slash_10 ;
           DUP @s ;
           CDAR %resolver ;
           DUUP @s ;
           CAR %sheet ;
           EXEC @r ;
           DUP @r ;
           IF_LEFT
             { DROP ; DUUP @s }
             { DROP ;
               DUUP @s ;
               DUP ;
               CAR %sheet ;
               SWAP ;
               CDR ;
               DUP ;
               CAR %resolver ;
               SWAP ;
               CDR ;
               CDR %bridge ;
               DUUUUP @r ;
               PAIR %result %bridge ;
               SWAP ;
               PAIR %resolver ;
               SWAP ;
               PAIR %sheet } ;
           DIP { DROP ; DROP } } ;
       DUUP @parameter ;
       IF_LEFT
         { RENAME @v_slash_18 ;
           DUUUUP @storage ;
           DUUUP @resolve ;
           DUUP @s ;
           CDR ;
           DUUUUP @v ;
           SOURCE @voter ;
           PAIR ;
           DUUUUP @s ;
           CAR %sheet ;
           PAIR ;
           DUP @sheet_voter_v ;
           CAR @sheet ;
           DUUP @sheet_voter_v ;
           CDAR @voter ;
           DUUUP @sheet_voter_v ;
           CDDR @v ;
           DUUUP @sheet ;
           DUUUP @voter ;
           GET ;
           IF_NONE
             { PUSH string "invalid voter" ; FAILWITH }
             { DUP @s ;
               IF_LEFT
                 { DROP ;
                   DUUUUP @sheet ;
                   DUUUP @v ;
                   IF_LEFT
                     { DROP ; PUSH string "cannot vote for nothing" ; FAILWITH }
                     { DROP ; DUUUP @v } ;
                   RENAME @v ;
                   SOME ;
                   DUUUUUP @voter ;
                   UPDATE }
                 { DROP ; PUSH string "already voted" ; FAILWITH } ;
               DIP { DROP } } ;
           DIP { DROP ; DROP ; DROP ; DROP } ;
           PAIR @s %sheet ;
           EXEC @s ;
           DUP @s ;
           DUUP @s ;
           DUP @s ;
           CDDDR %bridge ;
           CONTRACT @inst
             (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))) ;
           IF_NONE
             { NIL operation }
             { NIL operation ;
               SWAP ;
               PUSH mutez 0 ;
               DUUUUP ;
               CDDAR %result ;
               TRANSFER_TOKENS ;
               CONS } ;
           DIP { DROP } ;
           DIIP { DROP ; DROP ; DROP } ;
           RENAME @ops ;
           PAIR }
         { RENAME @__slash_24 ;
           DUUP @resolve ;
           DUUUUUP @storage ;
           EXEC @s ;
           DUP @s ;
           DUUP @s ;
           DUP @s ;
           CDDDR %bridge ;
           CONTRACT @inst
             (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative))) ;
           IF_NONE
             { NIL operation }
             { NIL operation ;
               SWAP ;
               PUSH mutez 0 ;
               DUUUUP ;
               CDDAR %result ;
               TRANSFER_TOKENS ;
               CONS } ;
           DIP { DROP } ;
           DIIP { DROP ; DROP } ;
           RENAME @ops ;
           PAIR } ;
       DIP { DROP ; DROP ; DROP } };
```
