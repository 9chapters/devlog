---
title: The Bridge
date: 2019-04-18 18:05:00
tag:
   - identity
   - hov
   - archive
---

## overview

### the challenge

Assuming we are writing a contract `C` that takes an address as input and send something to the contract at that address. Due to the lack of polymorphic type in Michelson, esp `CONTRACT 'p`, `C` can't just call arbitrary contract entry. Which means that, unless we, as writer of `C`, _knows_ the definition (i.e. parameter signature) of all potential targeting contracts, there is no way to cast the given input address into a proper contract instance.

For example, the signature of two following _handler_ might not be the same. And more important, we can't foresee their signatures and pre-defined inside `C`.

```mermaid
graph LR
   style X fill:#CCC

   X[someone] -.-> |address| C
   C(C) --> |trigger| H1
   C(C) --> |trigger| H2

   subgraph Bob's contracts
      H2(handler)
   end

   subgraph Alice's contracts
      H1(handler)
   end
```

### the method

To overcome that problem, there is one way to go - to public a simple but fixed contract signature: **Bridge**. All targeted contracts must have its parameter as typed as **Bridge** so that it can be casted successfully.

The _handlers_ in the following figure might have different signature, but those two bridges will have since they must saticfies the required signatures **Bridge**. And Since they are created by their owner, each of them will know how to call their _handler_ properly.

```mermaid
graph LR
   style X fill:#CCC

   X[someone] -.-> |address| C
   C --> |inform| B2
   C --> |inform| B1

   subgraph Bob's contracts
      B2(bridge)
      H2(handler)
      B2 --> |trigger| H2
   end

   subgraph Alice's contracts
      B1(bridge)
      H1(handler)
      B1 --> |trigger| H1
   end

```

## demo codes

To show a demo, we can firstly define the **Bridge** signature as

```
parameter (or unit (or unit unit));
```

And thus, people, who want to interact with `C`, must implement a contract with our **Bridge** signature and then send the address of it to `C`.

```
# myBridge.tz

parameter (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative)));
storage (pair :storage (address %handler) (bool %lock));
code { DUP ;
       DIP { CDR @s_slash_1 } ;
       CAR @t_slash_2 ;
       DUUP @s ;
       CDR %lock ;
       IF { DUUP @s ; NIL operation ; PAIR }
          { DUUP @s ;
            DUUUP @s ;
            CAR %handler ;
            CONTRACT @inst
              (or :_entries
                 (unit %_Liq_entry_foo)
                 (or :tTriple %_Liq_entry_goo
                    (unit %Undefined)
                    (or (unit %Positive) (unit %Negative)))) ;
            IF_NONE
              { NIL operation }
              { NIL operation ;
                SWAP ;
                PUSH mutez 0 ;
                DUUUUUP ;
                RIGHT % %_Liq_entry_goo unit ;
                TRANSFER_TOKENS ;
                CONS } ;
            RENAME @ops ;
            PAIR } ;
       DIP { DROP ; DROP } };
```

Additionally, we also need to write the real process on another contract which will be invoked by `myBridge.tz`.

```
# myHandler.tz

parameter
  (or :_entries
     (unit %_Liq_entry_foo)
     (or :tTriple %_Liq_entry_goo
        (unit %Undefined)
        (or (unit %Positive) (unit %Negative))));
storage (or :tTriple (unit %Undefined) (or (unit %Positive) (unit %Negative)));
code { DUP ;
       DIP { CDR @storage_slash_1 } ;
       CAR @parameter_slash_2 ;
       DUP @parameter ;
       IF_LEFT
         { DROP ; DUUP ; NIL operation ; PAIR }
         { RENAME @t_slash_5 ; NIL operation ; PAIR } ;
       DIP { DROP ; DROP } };

```
