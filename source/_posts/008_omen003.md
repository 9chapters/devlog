---
title: An omen of Nine Chapters (003)
date: 2019-06-27 17:00:00
tag:
   - archive
   - omen
   - teznames
   - dough
   - Tezos.ID
   - Baking Soda
---

> While you're turning the pages of the Nine Chapters,
> one sentence popped out: "More mysteries you have seen, more power you can feel."

## Updated

It's been a while since our last [_omen(002)_](https://9chapters.gitlab.io/devlog/2019/06/02/007_omen002/). But, _hey_, things are going well! Here is what we have done in last week.

+ Baking Soda
  + indexing doc by generated rules
+ dough
  + issued todo/tickets on gitlab
  + setup [users guide](https://dough.readthedocs.io/)
+ teznames
  + [70%] teznames’ roadmap on Miro
  + [WIP] issuing todo/tickets on gitlab
  + setup [users guide](https://teznames.readthedocs.io/)
+ devlog
  + devlogs integration and setup SPO
  + devlog template

## Upcoming

+ Baking Soda
  + release the very first version.
+ Tezos.ID
  + open the source codes of _Tezos.ID_ (ETA: 2019/6/27)
+ teznames/dough
  + upgrade into 9chsTNS-0.0.2
